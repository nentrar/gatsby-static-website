# Sekretna Biblioteczka	

Sekretna Biblioteczka is my **blog about books, philosophy and games** with posts written in Polish. Now on Wordpress, I plan to change it into the static site due to increased performance. And to learn something new, I choose Gatsby framework. 

If you want to read the blog the link is here: [sekretnabiblioteczka.pl](https://sekretnabiblioteczka.pl)